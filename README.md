# SMØLBOAT Keyboard

Keyboard modifications for the SMØLBOAT keyboard https://www.thingiverse.com/thing:3289175

## Things
For example:

* Lowered the place for a PCB to make room for Pro Micro and Teensy 2.0 boards
* Hole for reset button
* Riser insert to add even more headspace for Teeys 2.0 boards
* Insert for securing a Teensy 2.0 boards
* Insert for securing a reset button

Use crazy glue and hot glue to glue everything together, that's how it works for me.

## Designs
Designs are done in SketchUp 2017

![Four Smølboats](https://gitlab.com/3d-printing-for-the-public/smolboat-keyboard/-/raw/main/images/four%20smølboat%20keyboards.jpeg)]
